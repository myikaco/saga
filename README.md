# Sagas for Multi-service Transactions

Orchestration-based sagas with Golang.

## Publishing

To reset pkg.dev.go cache so that `go get` works on repos that use it:
```
git tag vX.X.X
git push --tags
curl https://sum.golang.org/lookup/gitlab.com/myikaco/saga@vX.X.X
```