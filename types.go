package saga

// SagaStep should return a non-nil error from Transaction func to commence UndoSaga(), which runs all compensating transactions starting from the current step and working backwards to the first step of the saga.
type SagaStep struct {
	Transaction             func(...interface{}) (interface{}, error)
	CompensatingTransaction func(...interface{}) (interface{}, error)
}

type Saga struct {
	//iterate through this slice to execute saga
	//	each step runs stream listen loop to wait for new OK response,
	//		if response OK, break listen loop and proceed with next step
	//			first OK response for each step from other svc includes message headers to listen for in next consecutive steps by the same svc
	//		if response FAIL, break all loops and start loop for compensating transactions
	Steps            []SagaStep
	currentStepIndex int
}
