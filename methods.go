package saga

import (
	"fmt"
)

var colorReset = "\033[0m"
var colorRed = "\033[31m"
var colorGreen = "\033[32m"
var colorYellow = "\033[33m"
var colorBlue = "\033[34m"
var colorPurple = "\033[35m"
var colorCyan = "\033[36m"
var colorWhite = "\033[37m"

func (saga *Saga) UndoSaga() {
	//start from current saga step and work backwards to run all compensating transactions
	for i := saga.currentStepIndex; i >= 0; i-- {
		saga.Steps[i].CompensatingTransaction()
	}
	saga.currentStepIndex = 0
}

// Execute runs each step of the saga, passing two arguments to each step.
// First arg is a map[string]interface{} with all the properties passed to Execute() call: botStream, consumerGroup, consumerID string AND also return vals of each previous saga step (key is the index of the stpe, beginniing at 0).
// Second arg is the funcArgs map[string]interface{} passed to Execute() call.
func (saga *Saga) Execute(botStream, consumerGroup, consumerID string, funcArgs map[string]interface{}) {
	transArgs := make(map[string]interface{})
	transArgs["botStream"] = botStream
	transArgs["consumerGroup"] = consumerGroup
	transArgs["consumerID"] = consumerID
	for i, step := range saga.Steps {
		saga.currentStepIndex = i
		//execute saga step
		//NOTE: wait for response at svc instance level
		undoSaga := false
		fmt.Printf(colorRed+"Starting saga step %v\n"+colorReset, i)
		res, err := step.Transaction(transArgs, funcArgs)
		fmt.Printf(colorRed+"Done saga step %v\n"+colorReset, i)
		if err != nil {
			fmt.Printf("Saga transaction err %s for step index %v\n", err.Error(), i)
			undoSaga = true
		}
		//add res of transaction to args map for next steps
		transArgs[fmt.Sprint(i)] = res

		//if failed at any point, begin compensating transaction loop
		if undoSaga {
			// TODO: mirror sagas.Execute() in reverse for UndoSaga
			saga.UndoSaga()
			break
		}
	}
	saga.currentStepIndex = 0
}
