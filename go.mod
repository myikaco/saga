module gitlab.com/myikaco/saga

go 1.16

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.5.0
	gitlab.com/myikaco/msngr v0.5.10
)
